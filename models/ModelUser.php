<?php

class User extends Model
{
    private $id;
    private $firstname;
    private $lastname;
    private $email;
    private $password;
    private $id_Persona;
    private $activated;
    private $personaName;

    const AUTHOR_ID = 2;

    public function __construct()
    {
        // Nous définissons la table par défaut de ce modèle
        $this->table = "users";

        // Nous ouvrons la connexion à la base de données
        $this->getConnection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getActivated()
    {
        return $this->activated;
    }

    /**
     * @param mixed $activated
     */
    public function setActivated($activated)
    {
        $this->activated = $activated;
    }

    /**
     * @return mixed
     */
    public function getIdPersona()
    {
        return $this->id_Persona;
    }

    /**
     * @param mixed $id_Persona
     */
    public function setIdPersona($id_Persona)
    {
        $this->id_Persona = $id_Persona;
    }

    public function getPersonaName()
    {
        $sql = "SELECT name FROM personas WHERE id=" . $this->getIdPersona();
        $query = $this->_connexion->prepare($sql);
        $query->execute();
        return ucfirst($query->fetchColumn());
    }

    public function canBeGranted()
    {
        if ($this->getIdPersona() == self::AUTHOR_ID) {
            return true;
        } else {
            return false;
        }
    }

    public function isActivated()
    {
        if ($this->getActivated() == 1):
            return true;
        else :
            return false;
        endif;
    }

    public function getAll($query = null)
    {
        $sql = "SELECT * FROM users";
        $query = $this->_connexion->prepare($sql);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_CLASS, "User");
    }

    public function create($user)
    {
        $sql = "INSERT INTO users (firstname, lastname, email, password, id_Persona, activated) VALUES (:firstname, :lastname, :email, :password, :id_Persona, 0)";
        $query = $this->_connexion->prepare($sql);
        $query->bindParam(':firstname', $user['firstname'], PDO::PARAM_STR);
        $query->bindParam(':lastname', $user['lastname'], PDO::PARAM_STR);
        $query->bindParam(':email', $user['email'], PDO::PARAM_STR);
        $query->bindParam(':password', password_hash($user['password'], PASSWORD_DEFAULT), PDO::PARAM_STR);
        $query->bindParam(':id_Persona', $user['id_Persona'], PDO::PARAM_STR);
        $query->execute();
        return $query->fetchObject("User");
    }

    public function login($user)
    {
        $sql = "SELECT * FROM users WHERE email=:email";
        $query = $this->_connexion->prepare($sql);
        $query->bindParam(':email', $user['email'], PDO::PARAM_STR);
        $query->execute();
        $user_bdd = $query->fetchObject("User");
        if ($user_bdd->activated) {
            if (password_verify($user['password'], $user_bdd->password)) {
                return $user_bdd;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function updatePersona(int $id, int $persona){
        $sql = "UPDATE users SET id_Persona=:id_Persona WHERE id=:id";
        $query = $this->_connexion->prepare($sql);
        $query->bindParam(':id', $id, PDO::PARAM_INT);
        $query->bindParam(':id_Persona', $persona, PDO::PARAM_INT);
        $query->execute();
        $user_bdd = $query->fetchObject("User");
        if ($user_bdd != null) {
            return true;
        } else {
            return false;
        }
    }

    public function updateStatus(int $id, int $status){
        $sql = "UPDATE users SET activated=:activated WHERE id=:id";
        $query = $this->_connexion->prepare($sql);
        $query->bindParam(':id', $id, PDO::PARAM_INT);
        $query->bindParam(':activated', $status, PDO::PARAM_INT);
        $query->execute();
        $user_bdd = $query->fetchObject("User");
        if ($user_bdd != null) {
            return true;
        } else {
            return false;
        }
    }

    public function __toString()
    {
        return $this->firstname . " " . $this->lastname;
    }
}
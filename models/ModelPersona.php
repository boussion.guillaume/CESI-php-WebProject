<?php

class Persona extends Model
{
    private $id;
    private $name;

    public function __construct()
    {
        // Nous définissons la table par défaut de ce modèle
        $this->table = "personas";

        // Nous ouvrons la connexion à la base de données
        $this->getConnection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getAll($query = null)
    {
        $sql = "SELECT * FROM personas";
        $query = $this->_connexion->prepare($sql);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_CLASS, "Persona");

    }

//    public function __toString()
//    {
//        return $this->name;
//    }
}

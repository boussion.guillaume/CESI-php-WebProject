<?php

class Article extends Model
{
    private $id;

    private $title;
    private $description;
    private $image;
    private $publish_date;
    private $published;
    private $id_User;
    // Liste des mots à exclure de la recherche pour éviter de fausser les résultats

    const TERMS_TO_AVOID = array("de", "le", "du", "que", "qui", "et");
    const MAX_TERMS_TO_SEARCH = 15;
    const TERM_MAX_LENGTH = 30;

    public function __construct()
    {
        // Nous définissons la table par défaut de ce modèle
        $this->table = "articles";

        // Nous ouvrons la connexion à la base de données
        $this->getConnection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getShortDescription()
    {
        return substr($this->description, 0, 150);
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getPublishDate()
    {
        return date('d/m/Y à H:i', strtotime($this->publish_date));
    }

    /**
     * @param mixed $publish_date
     */
    public function setPublishDate($publish_date)
    {
        $this->publish_date = $publish_date;
    }

    /**
     * @return mixed
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @param mixed $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    public function isPublished()
    {
        if ($this->getPublished() == 1):
            return true;
        else :
            return false;
        endif;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->id_User;
    }

    /**
     * @param mixed $id_User
     */
    public function setIdUser($id_User)
    {
        $this->id_User = $id_User;
    }

    public function getAuthorName()
    {
        $sql = "SELECT firstname, lastname FROM users WHERE id=" . $this->getIdUser();
        $query = $this->_connexion->prepare($sql);
        $query->execute();
        $fetchResult = $query->fetch();
        return $fetchResult['firstname'] . ' ' . $fetchResult['lastname'];
    }

    public function create($article)
    {
        $sql = "INSERT INTO " . $this->table . " (title, description, image, publish_date, published, idUser) VALUES (:title, :description, :image, :publish_date, :published, :idUser)";
        $query = $this->_connexion->prepare($sql);
        $query->bindParam(':title', $article->title, PDO::PARAM_STR);
        $query->bindParam(':description', $article->description, PDO::PARAM_STR);
        $query->bindParam(':image', $article->image, PDO::PARAM_STR);
        $query->bindParam(':idUser', $article->id_User, PDO::PARAM_INT);
        $query = $this->_connexion->prepare($sql);
        $query->execute();
        return $query->fetchObject("Article");
    }

    /**
     * Méthode permettant d'obtenir un enregistrement de la table articles en fonction d'un id
     *
     * @return void
     */
    public function getOne($id)
    {
        $sql = "SELECT * FROM " . $this->table . " WHERE id=" . $id;
        $query = $this->_connexion->prepare($sql);
        $query->execute();
        return $query->fetchObject("Article");
    }

    /**
     * Méthode permettant d'obtenir tous les enregistrements de la table articles
     *
     * @return void
     */
    public function getAll(int $first, int $per_page, array $query = array())
    {
        $pagination_query = "LIMIT " . $first . ", " . $per_page . ";";
        $sql = "SELECT * FROM " . $this->table;
        if ($query != null) {
            $i = 0;
            foreach ($query as $key => $value) {
                if ($i == 0) {
                    $sql = $sql . " WHERE " . $key . "=" . strval($value);
                } else {
                    $sql = $sql . " AND " . $key . "=" . strval($value);
                }
                $i++;
            }
        }
        $sql = $sql . " ORDER BY publish_date DESC " . $pagination_query;
        $query = $this->_connexion->prepare($sql);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_CLASS, "Article");
    }

    public function getAllByIds(array $ids){

        $arrayOfKeys = array_keys($ids);
        $idsParam = implode($arrayOfKeys, ',');
        $sql = "SELECT * FROM " . $this->table . " WHERE id in (" . $idsParam . ")";
        $query = $this->_connexion->prepare($sql);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_CLASS, "Article");
    }

    public function countArticles(array $query = array())
    {
        $sql = "SELECT COUNT(*) as nbArticles FROM " . $this->table;
        if ($query != null) {
            $i = 0;
            foreach ($query as $key => $value) {
                if ($i == 0) {
                    $sql = $sql . " WHERE " . $key . "=" . strval($value);
                } else {
                    $sql = $sql . " AND " . $key . "=" . strval($value);
                }
                $i++;
            }
        }
        $query = $this->_connexion->prepare($sql);
        $query->execute();
        return $query->fetch()['nbArticles'];
    }

    public function deleteOne()
    {
        $sql = "DELETE FROM " . $this->table . " WHERE id =" . $this->getId();
        $query = $this->_connexion->prepare($sql);
        $query->execute();
    }

    public function updateArticleById(string $title, $description, $imageName, $id)
    {
        $sql = "UPDATE " . $this->table . " SET title=:title, description=:description, image=:image WHERE id=:id";
        $query = $this->_connexion->prepare($sql);
        $query->bindParam(':title', $title, PDO::PARAM_STR);
        $query->bindParam(':description', $description, PDO::PARAM_STR);
        $query->bindParam(':image', $imageName, PDO::PARAM_STR);
        $query->bindParam(':id', $id, PDO::PARAM_INT);
        $query->execute();
    }

    public function createNew(string $title, $description, $imageName, $userId)
    {
        $sql = "INSERT INTO " . $this->table . " (title, description, image, publish_date, published, id_User) VALUES (:title, :description, :image, :publish_date, :published, :id_User)";
        $query = $this->_connexion->prepare($sql);
        $query->bindParam(':title', $title, PDO::PARAM_STR);
        $query->bindParam(':description', $description, PDO::PARAM_STR);
        $query->bindParam(':image', $imageName, PDO::PARAM_STR);
        $query->bindParam(':id_User', $userId, PDO::PARAM_STR);
        $query->bindValue(':publish_date', date("Y-m-d H:i:s"));
        $query->bindValue(':published', 0);
        $query->execute();
    }

    public function searchInArticles(string $terms)
    {
        // Parse la liste des termes contenus dans la recherche
        $arrayOfTermsToFilter = explode(" ", $terms);
        $arrayOfTermsFiltered = array();
        $counterOfIteration = 0;

        if (count($arrayOfTermsToFilter) > 0) {
            // on traite la recherche avant de l'envoyer en bdd
            foreach ($arrayOfTermsToFilter as $term) {

                // passe à l'itération suivante si un terme à éviter est trouvé ou que le terme à rechercher est trop long
                if (in_array($term, self::TERMS_TO_AVOID) || strlen($term) >= self::TERM_MAX_LENGTH) {
                    continue;
                }
                // sinon on ajoute au tableau des termes à rechercher
                $arrayOfTermsFiltered[] = $term;

                // on vérifie qu'on n'excède pas le nombre de termes max à chercher
                if ($counterOfIteration >= self::MAX_TERMS_TO_SEARCH) {
                    break;
                }

                // on incrémente le nombre de termes ajoutés
                $counterOfIteration++;
            }


            if (count($arrayOfTermsFiltered) > 0) {

                // on recherche chaque mot dans la base, et on instancie un score à chaque fois qu'un article possède un terme recherché
                $arrayOfScore = array();
                foreach ($arrayOfTermsFiltered as $termFiltered){
                    $sql = "SELECT * FROM ". $this->table . " WHERE title LIKE '%" . $termFiltered . "%' OR description LIKE '%" . $termFiltered . "%'";
                    $query = $this->_connexion->prepare($sql);
                    $query->execute();

                    // On récupère l'ensemble des ids des articles qui contiennent le terme et on ajoute au tableau des scores
                    $articleIdsReturned = $query->fetchAll();

                    // on incrémente le tableau des scores avec +1 à chaque ID d'article trouvé
                    foreach ($articleIdsReturned as $articleId){
                        $arrayOfScore[$articleId['id']] += 1;
                    }
                }
                // on trie le tableau par score
                arsort($arrayOfScore, SORT_NUMERIC);

                return $arrayOfScore;
            }
        }

    }
}
<?php

abstract class Controller
{

    public function __construct()
    {
        // Enable PHP Session
        if (empty($_SESSION))
            @session_start();
    }

    /**
     * Permet de charger un modèle
     *
     * @param string $model
     * @return void
     */
    public function loadModel(string $model)
    {
        // On va chercher le fichier correspondant au modèle souhaité
        require_once(ROOT . 'models/Model' . $model . '.php');

        // On crée une instance de ce modèle. Ainsi "Article" sera accessible par $this->Article
        $this->$model = new $model();
    }
}
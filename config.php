<?php
return array(
    "database" => array(
        "default" => array(
            "name" => "projet_web",
            "host" => "db",
            "user" => "admin",
            "pass" => "projetweb2020"
        )
    ),
    "texts" => array(
        "title" => "Blog PHP"
    ),
    "roots" => array(
        "project" => "/",
        "statics" => "/static",
        "data" => "/data/img"
    )
);
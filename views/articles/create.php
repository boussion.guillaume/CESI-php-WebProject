<div class="container">
    <h1 class="text-center mt-5 mb-3">Ajouter un article</h1>
    <form action="/articles/createNewArticle" method="post" enctype="multipart/form-data">
        <div class="modal-body">

            <div class="form-group">
                <label for="recipient-name" class="col-form-label">Titre :</label>
                <input type="text"
                       class="form-control" name="article-title">
            </div>
            <div class="form-group">
                <label for="message-text" class="col-form-label">Contenu :</label>
                <textarea class="form-control"
                          id="article-content"
                          name="article-content"></textarea>
            </div>
            <div class="form-group">
                <label for="message-text" class="col-form-label">Image :</label>
                <div class="custom-file">
                    <input type="file" name="article-image"
                           accept=".jpg,.jpeg,.png,.gif" class="custom-file-input"
                           id="article-image">
                    <label class="custom-file-label" for="customFile">Ajouter une image</label>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Ajouter l'article</button>
        </div>
    </form>
</div>

<script>
    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-10 mx-auto my-4">

            <!-- Title -->
            <h1 class="mt-4"><?= $article->getTitle(); ?></h1>

            <hr>

            <p><a href="/">Blog</a> / <?= $article->getTitle(); ?></p>
            <p>Posté le <?= $article->getPublishDate(); ?> par <?= $article->getAuthorName(); ?></p>
            <hr>


            <!-- Preview Image -->
            <img class="img-fluid rounded" src="<?= $article->getImage(); ?>" alt="">

            <hr>

            <!-- Post Content -->
            <p class="lead"><?= $article->getDescription(); ?></p>

        </div>
    </div>
    <!-- /.row -->

</div>
<!-- /.container -->
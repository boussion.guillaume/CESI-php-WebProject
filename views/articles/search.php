<!-- Page Content -->
<div class="container">

    <div class="row">

        <h4 class="mt-4">Résultat de la recherche pour : <?= $request ?></h4>

        <!-- Blog Entries Column -->
        <div class="col-md-8 my-4">
            <?php foreach ($search as $article): ?>
                <!-- Blog Post -->
                <div class="card mb-4">
                    <a href="/articles/detail/<?php echo $article->getId(); ?>">
                        <img class="card-img-top" src="<?php echo $article->getImage(); ?>" alt="Card image cap">
                    </a>
                    <div class="card-body">
                        <h2 class="card-title"><?php echo $article->getTitle(); ?></h2>
                        <p class="card-text"><?php echo $article->getShortDescription(); ?>...
                            <a href="/articles/detail/<?php echo $article->getId(); ?>">Lire plus</a>
                        </p>
                    </div>
                    <div class="card-footer text-muted">Posté le <?php echo $article->getPublishDate(); ?></div>
                </div>

            <?php endforeach ?>
        </div>
        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

            <!-- Search Widget -->
            <div class="card my-4">
                <h5 class="card-header">Recherche</h5>
                <div class="card-body">
                    <div class="input-group">
                        <form action="/articles/search" method="post">
                            <input type="text" class="form-control" name="search" placeholder="Rechercher...">
                            <span class="input-group-append">
                                <button class="btn btn-secondary d-inline" type="submit">Go</button>
                            </span>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

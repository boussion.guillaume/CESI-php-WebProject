<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8 my-4">
            <!---->
            <!--            <h1 class="my-4">Page Heading-->
            <!--                <small>Secondary Text</small>-->
            <!--            </h1>-->
            <?php foreach ($articles as $article): ?>
                <!-- Blog Post -->
                <div class="card mb-4">
                    <a href="/articles/detail/<?php echo $article->getId(); ?>">
                        <img class="card-img-top" src="<?php echo $article->getImage(); ?>" alt="Card image cap">
                    </a>
                    <div class="card-body">
                        <h2 class="card-title"><?php echo $article->getTitle(); ?></h2>
                        <p class="card-text"><?php echo $article->getShortDescription(); ?>...
                            <a href="/articles/detail/<?php echo $article->getId(); ?>">Lire plus</a>
                        </p>
                    </div>
                    <div class="card-footer text-muted">Posté le <?php echo $article->getPublishDate(); ?></div>
                </div>

            <?php endforeach ?>

            <!-- Pagination -->
            <nav>
                <ul class="pagination justify-content-center mb-4">
                    <!-- Lien vers la page précédente (désactivé si on se trouve sur la 1ère page) -->
                    <li class="page-item <?= ($currentPage == 1) ? "disabled" : "" ?>">
                        <a href="./?page=<?= $currentPage - 1 ?>" class="page-link">Précédente</a>
                    </li>
                    <?php for ($page = 1; $page <= $pages; $page++): ?>
                        <!-- Lien vers chacune des pages (activé si on se trouve sur la page correspondante) -->
                        <li class="page-item <?= ($currentPage == $page) ? "active" : "" ?>">
                            <a href="./?page=<?= $page ?>" class="page-link"><?= $page ?></a>
                        </li>
                    <?php endfor ?>
                    <!-- Lien vers la page suivante (désactivé si on se trouve sur la dernière page) -->
                    <li class="page-item <?= ($currentPage == $pages) ? "disabled" : "" ?>">
                        <a href="./?page=<?= $currentPage + 1 ?>" class="page-link">Suivante</a>
                    </li>
                </ul>
            </nav>
        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

            <!-- Search Widget -->
            <div class="card my-4">
                <h5 class="card-header">Recherche</h5>
                <div class="card-body">
                    <div class="input-group">
                        <form class="d-inline" action="/articles/search" method="post">
                            <input type="text" class="form-control" name="search" placeholder="Rechercher...">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">Go</button>
                            </span>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Categories Widget -->
            <div class="card my-4">
                <h5 class="card-header">Catégories</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="#">Web Design</a>
                                </li>
                                <li>
                                    <a href="#">HTML</a>
                                </li>
                                <li>
                                    <a href="#">Freebies</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="#">JavaScript</a>
                                </li>
                                <li>
                                    <a href="#">CSS</a>
                                </li>
                                <li>
                                    <a href="#">Tutorials</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->
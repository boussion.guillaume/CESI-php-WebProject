<?php echo $test; ?>
<div class="container">
    <div class="row">
        <div class="col-md-6 my-4 mx-auto">
            <div class="panel panel-login">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-6 text-center p-0">
                            <a href="#" class="active d-block switcher-button p-2" id="login-form-link">Login</a>
                        </div>
                        <div class="col-6 text-center p-0">
                            <a href="#" id="register-form-link" class="d-block switcher-button p-2">Register</a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12 p-3">
                            <form id="login-form" action="/users/login" method="post">
                                <div class="form-group">
                                    <label for="email">Adresse email</label>
                                    <input type="email" class="form-control" id="email" name="email"
                                           placeholder="Entrez votre email">
                                </div>
                                <div class="form-group">
                                    <label for="password">Mot de passe</label>
                                    <input type="password" class="form-control" id="password" name="password"
                                           placeholder="Entrez votre mot de passe">
                                </div>
                                <div id="error-message" class="text-danger"></div>
                                <button type="submit" class="btn btn-primary">Connexion</button>
                            </form>
                            <form id="register-form" action="/users/registration" method="post" style="display: none;">
                                <div class="form-group">
                                    <label for="email">Adresse email</label>
                                    <input type="email" class="form-control" id="email" name="email"
                                           placeholder="Entrez votre email">
                                </div>
                                <div class="row mb-4">
                                    <div class="col">
                                        <label for="email">Prénom</label>
                                        <input id="Prénom" name="firstname" type="text" class="form-control"
                                               placeholder="Entrez votre prénom">
                                    </div>
                                    <div class="col">
                                        <label for="email">Nom</label>
                                        <input id="Nom" name="lastname" type="text" class="form-control"
                                               placeholder="Entrez votre nom">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password">Mot de passe</label>
                                    <input type="password" class="form-control" id="password" name="password"
                                           placeholder="Entrez votre mot de passe">
                                </div>
                                <button type="submit" class="btn btn-primary">Inscription</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#login-form-link').click(function (e) {
            $("#login-form").delay(100).fadeIn(100);
            $("#register-form").fadeOut(100);
            $('#register-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });
        $('#register-form-link').click(function (e) {
            $("#register-form").delay(100).fadeIn(100);
            $("#login-form").fadeOut(100);
            $('#login-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });
        getUrlVars();
    });

    // Read a page's GET URL variables and return them as an associative array.
    function getUrlVars() {
        const urlParams = new URLSearchParams(window.location.search);
        const myParam = urlParams.get('error');
        if (myParam != null) {
            $('#error-message').html('<p>' + myParam + '</p>');
        }
    }
</script>
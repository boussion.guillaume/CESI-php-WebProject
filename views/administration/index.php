<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky mt-3">
                <ul class="nav flex-column">
                    <?php if ($_SESSION['id_Persona'] == 1): ?>
                        <li class="nav-item">
                            <a class="nav-link active" href="/administration">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-home mr-2">
                                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                                </svg>
                                Utilisateurs
                            </a>
                        </li>
                    <?php endif; ?>
                    <ul class="nav flex-column mb-2">
                        <li class="nav-item">
                            <a class="nav-link" href="/administration/articles">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-file-text mr-2">
                                    <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                    <polyline points="14 2 14 8 20 8"></polyline>
                                    <line x1="16" y1="13" x2="8" y2="13"></line>
                                    <line x1="16" y1="17" x2="8" y2="17"></line>
                                    <polyline points="10 9 9 9 8 9"></polyline>
                                </svg>
                                Gestion des articles
                            </a>
                        </li>
                    </ul>
            </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            <div class="chartjs-size-monitor"
                 style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                <div class="chartjs-size-monitor-expand"
                     style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                    <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                </div>
                <div class="chartjs-size-monitor-shrink"
                     style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                    <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                </div>
            </div>
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                <h1 class="h2">Panel administration</h1>

            </div>

            <h4 class="mt-4 mb-4">Liste des utilisateurs</h4>
            <div class="table-responsive mb-4">
                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nom</th>
                        <th>Prénom</th>
                        <th>Email</th>
                        <th>Profil</th>
                        <th class="text-center">Statut</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($users as $user): ?>
                        <tr id="<?= $user->getId(); ?>">
                            <td><?= $user->getId(); ?></td>
                            <td><?= $user->getLastname(); ?></td>
                            <td><?= $user->getFirstname(); ?></td>
                            <td><?= $user->getEmail(); ?></td>
                            <td>
                                <form class="grantUser-form" action="/administration/grantUser" method="post">
                                    <input type="hidden" value="<?= $user->getId(); ?>" name="user_id">
                                    <select name="persona" class="form-control"
                                            onchange="submitForm(<?= $user->getId(); ?>, 'grantUser-form')"
                                        <?= $_SESSION['id'] == $user->getId() ? 'disabled' : ''; ?>>
                                        <?php foreach ($personas as $persona): ?>
                                            <option value="<?= $persona->getId(); ?>"
                                                <?= $persona->getId() == $user->getIdPersona() ? 'selected' : ''; ?>>
                                                <?= $persona->getName(); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </form>
                            </td>
                            <td class="text-center">
                                <form class="activateUser-form" action="/administration/activateUser" method="post">
                                    <input type="hidden" value="<?= $user->getId(); ?>" name="user_id">
                                    <select name="status" class="form-control"
                                            onchange="submitForm(<?= $user->getId(); ?>, 'activateUser-form')"
                                        <?= $_SESSION['id'] == $user->getId() ? 'disabled' : ''; ?>>
                                        <option value="0" <?= $user->isActivated() == false ? 'selected' : ''; ?>>
                                            Désactivé
                                        </option>
                                        <option value="1" <?= $user->isActivated() == true ? 'selected' : ''; ?>>
                                            Activé
                                        </option>
                                    </select>
                                </form>
                            </td>
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </main>
    </div>
</div>

<script>
    function submitForm(id, formClass) {
        $('tr#' + id + ' form.' + formClass).submit();
    }
</script>
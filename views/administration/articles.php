<?php $config = include('/var/www/html/config.php'); ?>
<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky mt-3">
                <ul class="nav flex-column">
                    <?php if ($_SESSION['id_Persona'] == 1): ?>
                        <li class="nav-item">
                            <a class="nav-link active" href="/administration">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-home mr-2">
                                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                                </svg>
                                Utilisateurs
                            </a>
                        </li>
                    <?php endif; ?>
                    <ul class="nav flex-column mb-2">
                        <li class="nav-item">
                            <a class="nav-link" href="/administration/articles">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-file-text mr-2">
                                    <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                    <polyline points="14 2 14 8 20 8"></polyline>
                                    <line x1="16" y1="13" x2="8" y2="13"></line>
                                    <line x1="16" y1="17" x2="8" y2="17"></line>
                                    <polyline points="10 9 9 9 8 9"></polyline>
                                </svg>
                                Gestion des articles
                            </a>
                        </li>
                    </ul>
            </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            <div class="chartjs-size-monitor"
                 style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                <div class="chartjs-size-monitor-expand"
                     style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                    <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                </div>
                <div class="chartjs-size-monitor-shrink"
                     style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                    <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                </div>
            </div>
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                <h1 class="h2">Panel administration</h1>

            </div>

            <h4 class="mt-4 mb-4">Liste des utilisateurs</h4>
            <div class="table-responsive mb-4">
                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nom article</th>
                        <th>Date publication</th>
                        <th>Auteur</th>
                        <th>Statut</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($articles)): ?>
                        <?php foreach ($articles as $article): ?>
                            <tr>
                                <!-- Titre, date publi, auteur & statut -->
                                <td class="align-middle"><?= $article->getId(); ?></td>
                                <td class="align-middle"><?= $article->getTitle(); ?></td>
                                <td class="align-middle"><?= $article->getPublishDate(); ?></td>
                                <td class="align-middle"><?= $article->getAuthorName(); ?></td>

                                <td class="align-middle">
                                    <?php if ($article->isPublished()): ?>
                                        <span>Publié</span>
                                    <?php else : ?>
                                        <span>En attente de validation</span>
                                    <?php endif; ?>
                                </td>

                                <td class="align-middle"><a data-toggle="modal"
                                                            data-target="#modal-<?= $article->getId(); ?>"><i
                                                class="fas fa-edit"></i></a></td>
                                <td class="align-middle">
                                    <form action="/administration/deleteArticle" class="d-inline" method="post">
                                        <input type="hidden" name="articleIdToDelete" value="<?= $article->getId(); ?>">
                                        <button class="btn btn-danger" type="submit">
                                            <i class="fas fa-trash-alt color-danger"></i>
                                        </button>
                                    </form>
                                </td>

                            </tr>


                            <div class="modal fade" id="modal-<?= $article->getId(); ?>" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Edition de l'article</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form action="/administration/updateArticle" method="post"
                                              enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <input type="hidden" name="article-id"
                                                       value="<?= $article->getId(); ?>">

                                                <div class="form-group">
                                                    <label for="recipient-name" class="col-form-label">Titre :</label>
                                                    <input value="<?= $article->getTitle(); ?>" type="text"
                                                           class="form-control" name="article-title">
                                                </div>
                                                <div class="form-group">
                                                    <label for="message-text" class="col-form-label">Contenu :</label>
                                                    <textarea class="form-control"
                                                              id="article-content"
                                                              name="article-content"> <?= $article->getDescription(); ?></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="message-text" class="col-form-label">Image :</label>
                                                    <img class="w-100"
                                                         src="<?php echo $config['roots']['data'] . '/' . $article->getImage(); ?>">
                                                </div>
                                                <div class="form-group">
                                                    <div class="custom-file">
                                                        <input type="file" name="article-image"
                                                               accept=".jpg,.jpeg,.png,.gif" class="custom-file-input"
                                                               id="article-image">
                                                        <label class="custom-file-label" for="customFile">Modifier
                                                            l'image</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                    Fermer
                                                </button>
                                                <button type="submit" class="btn btn-primary">Mettre à jour</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>

                        <?php endforeach ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="7">Aucun article pour le moment</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
                <!-- Pagination -->
                <nav>
                    <ul class="pagination justify-content-center mb-4">
                        <!-- Lien vers la page précédente (désactivé si on se trouve sur la 1ère page) -->
                        <li class="page-item <?= ($currentPage == 1) ? "disabled" : "" ?>">
                            <a href="/administration/articles/?page=<?= $currentPage - 1 ?>" class="page-link">Précédente</a>
                        </li>
                        <?php for ($page = 1; $page <= $pages; $page++): ?>
                            <!-- Lien vers chacune des pages (activé si on se trouve sur la page correspondante) -->
                            <li class="page-item <?= ($currentPage == $page) ? "active" : "" ?>">
                                <a href="/administration/articles/?page=<?= $page ?>" class="page-link"><?= $page ?></a>
                            </li>
                        <?php endfor ?>
                        <!-- Lien vers la page suivante (désactivé si on se trouve sur la dernière page) -->
                        <li class="page-item <?= ($currentPage == $pages) ? "disabled" : "" ?>">
                            <a href="/administration/articles/?page=<?= $currentPage + 1 ?>"
                               class="page-link">Suivante</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </main>
    </div>
</div>

<script>
    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>
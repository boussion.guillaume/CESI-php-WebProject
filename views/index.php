<?php
$config = include('/var/www/html/config.php');
?>
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $config['texts']['title']; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $config['roots']['statics']; ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $config['roots']['statics']; ?>/vendor/font-awesome/css/all.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo $config['roots']['statics']; ?>/css/blog-home.css" rel="stylesheet">
    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo $config['roots']['statics']; ?>/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo $config['roots']['statics']; ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="/">Blog</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <?php if ($_SESSION['is_logged']): ?>
                    <li class="nav-item ml-2">
                        <a class="btn btn-light" href="/administration">Administration</a>
                        <form action="/users/logout" method="post" class="d-inline">
                            <button type="submit" class="btn btn-danger">Déconnexion</button>
                        </form>
<!--                        <a class="btn btn-danger" href="#">Déconnexion</a>-->
                    </li>
                <?php else: ?>
                    <li class="nav-item ml-2">
                        <a class="btn btn-light" href="/users">Connexion</a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>

<?= $content ?>

</body>

<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Arthur Laveau / Guillaume Boussion 2020</p>
    </div>
    <!-- /.container -->
</footer>


</html>

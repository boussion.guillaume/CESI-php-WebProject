<?php

require_once 'models/ModelUser.php';

class Users extends Controller
{
    /**
     * Cette méthode affiche la liste des users
     *
     * @return void
     */
    public function index()
    {
        if ($_SESSION['is_logged']) {
            header('Location: /administration');
        }

        // On instancie le modèle "User"
        $this->loadModel('User');

        // On démarre le buffer de sortie
        ob_start();

        // On génère la vue
        require_once(ROOT . 'views/users/index.php');

        // On stocke le contenu dans $content
        $content = ob_get_clean();

        // On fabrique le "template"
        require_once(ROOT . 'views/index.php');
    }

    /**
     * Cette méthode inscrit un utilisateur dans la base de données
     *
     * @return void
     */
    public function registration()
    {
        $user = array(
            'firstname' => $_POST['firstname'],
            'lastname' => $_POST['lastname'],
            'email' => $_POST['email'],
            'password' => $_POST['password'],
            'id_Persona' => 2
        );
        $userInstance = new User();
        $new_user = $userInstance->create($user);
        if (!empty($new_user)) {
            $_SESSION['id'] = $new_user->getId();
            $_SESSION['firstname'] = $new_user->getFirstname();
            $_SESSION['lastname'] = $new_user->getLastname();
            $_SESSION['email'] = $new_user->getEmail();
            $_SESSION['id_Persona'] = $new_user->getIdPersona();
            $_SESSION['activated'] = $new_user->getActivated();
            $_SESSION['is_logged'] = true;
            header('Location: /administration');
        } else {
            header('Location: /users');
        }
    }

    /**
     * Cette méthode connecte un utilisateur
     *
     * @return void
     */
    public function login()
    {
        if (isset($_POST['email']) && isset($_POST['password'])) {
            $user = array(
                'email' => $_POST['email'],
                'password' => $_POST['password']
            );
            $userInstance = new User();
            $logged_user = $userInstance->login($user);
            if ($logged_user) {
                $_SESSION['id'] = $logged_user->getId();
                $_SESSION['firstname'] = $logged_user->getFirstname();
                $_SESSION['lastname'] = $logged_user->getLastname();
                $_SESSION['email'] = $logged_user->getEmail();
                $_SESSION['id_Persona'] = $logged_user->getIdPersona();
                $_SESSION['activated'] = $logged_user->getActivated();
                $_SESSION['is_logged'] = true;
                header('Location: /administration');
            } else {
                header('Location: /users/?error=Identifiant ou mot de passe incorrecte.');
            }
        }

    }

    public function logout()
    {
        session_unset();
        session_write_close();
        header('Location: /users');
    }


}
<?php

require_once 'models/ModelUser.php';

class Administration extends Controller
{
    const PER_PAGE = 2;

    public function index()
    {
        if (!$_SESSION['is_logged']) {
            header('Location: /users');
        }

        if ($_SESSION['is_logged'] && $_SESSION['id_Persona'] == 2) {
            header('Location: /administration/articles');
        }

        // On instancie le modèle "User"
        $this->loadModel('User');
        $this->loadModel('Persona');

        // On démarre le buffer de sortie
        ob_start();

        // On stocke la liste des users dans $users
        $users = $this->User->getAll();
        $personas = $this->Persona->getAll();

        // On génère la vue
        require_once(ROOT . 'views/administration/index.php');

        // On stocke le contenu dans $content
        $content = ob_get_clean();

        // On fabrique le "template"
        require_once(ROOT . 'views/index.php');
    }

    public function articles()
    {
        if (!$_SESSION['is_logged']) {
            header('Location: /users');
        }

        // On détermine sur quelle page on se trouve
        if (parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY) != null) {
            $currentPage = explode("=", parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY));
            if ($currentPage[0] == 'page') {
                $currentPage = (int)strip_tags($currentPage[1]);
            } else {
                $currentPage = 1;
            }
        } else {
            $currentPage = 1;
        }
        $queries = array();
        parse_str($_SERVER['QUERY_STRING'], $queries);

        // On instancie le modèle "Article"
        $this->loadModel('Article');

        // On démarre le buffer de sortie
        ob_start();


        if (isset($_SESSION['id_Persona']) && $_SESSION['id_Persona'] == 1) {
            // Compte le nombre d'article en base
            $nb_articles = (int) $this->Article->countArticles();

            // Calcul du nombre de pages à afficher
            $pages = ceil($nb_articles / self::PER_PAGE);

            // Calcul du premier article de la page
            $first = ($currentPage * self::PER_PAGE) - self::PER_PAGE;

            // On stocke la liste des articles dans $articles
            $articles = $this->Article->getAll($first, self::PER_PAGE);
        } else {
            $query = array("id_User" => $_SESSION['id']);
            // Compte le nombre d'article en base
            $nb_articles = (int) $this->Article->countArticles($query);

            // Calcul du nombre de pages à afficher
            $pages = ceil($nb_articles / self::PER_PAGE);

            // Calcul du premier article de la page
            $first = ($currentPage * self::PER_PAGE) - self::PER_PAGE;
            $articles = $this->Article->getAll($first, self::PER_PAGE, $query);
        }

        // On génère la vue
        require_once(ROOT . 'views/administration/articles.php');

        // On stocke le contenu dans $content
        $content = ob_get_clean();

        // On fabrique le "template"
        require_once(ROOT . 'views/index.php');
    }


    public function deleteArticle(){

        // On instancie le modèle "Article"
        $this->loadModel('Article');

        $articleIdToDelete =  $_POST['articleIdToDelete'];
        if(isset($_POST['articleIdToDelete']) && is_numeric($_POST['articleIdToDelete'])) {
            $article = new Article();
            $article->setId($_POST['articleIdToDelete']);

            $article->deleteOne();
            header('Location: /administration/');
        }
    }

    public function updateArticle()
    {

        // On instancie le modèle "Article"
        $this->loadModel('Article');

        $config = include('/var/www/html/config.php');
        $error = "";

        if (isset($_FILES['article-image']) && $_FILES['article-image']['error'] === UPLOAD_ERR_OK) {

            // Traitement de l'image uploadée
            $fileTmpPath = $_FILES['article-image']['tmp_name'];
            $fileName = $_FILES['article-image']['name'];
            $fileSize = $_FILES['article-image']['size'];
            $fileType = $_FILES['article-image']['type'];
            $fileNameCmps = explode(".", $fileName);
            $fileExtension = strtolower(end($fileNameCmps));

            $newFileName = md5(time() . $fileName) . '.' . $fileExtension;

            // directory in which the uploaded file will be moved
            $uploadFileDir = '/var/www/html/' . $config['roots']['data'] . '/';
            $dest_path = $uploadFileDir . $newFileName;

            if (move_uploaded_file($fileTmpPath, $dest_path)) {
                $message = "L'image a bien été upload";

                // Alors on update l'Article
                $articleUpdated = new Article();
                $articleUpdated->updateArticleById($_POST['article-title'], $_POST['article-content'], $newFileName, $_POST['article-id']);
            } else {
                $error = "Une erreur est survenue lors de la tentative de modification de l'article";
            }
        } else {
            $error = "Une erreur est survenue lors de la tentative de modification de l'article";
        }
    }

    public function grantUser()
    {
        if (isset($_POST['user_id']) && isset($_POST['persona'])) {
            $user = new User();
            $updated = $user->updatePersona($_POST['user_id'], $_POST['persona']);
            header('Location: /administration');
        }
    }

    public function activateUser()
    {
        if (isset($_POST['user_id']) && isset($_POST['status'])) {
            $user = new User();
            $updated = $user->updateStatus($_POST['user_id'], $_POST['status']);
            header('Location: /administration');
        }
    }

}
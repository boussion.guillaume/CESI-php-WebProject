<?php

class Articles extends Controller
{
    const PER_PAGE = 2;

    /**
     * Cette méthode affiche la liste des articles
     *
     * @return void
     */
    public function index()
    {
        // On détermine sur quelle page on se trouve
        if (isset($_GET['page']) && !empty($_GET['page'])) {
            $currentPage = (int)strip_tags($_GET['page']);
        } else {
            $currentPage = 1;
        }

        // On instancie le modèle "Article"
        $this->loadModel('Article');

        // On démarre le buffer de sortie
        ob_start();

        $query = array("published" => 1);

        // Compte le nombre d'article en base
        $nb_articles = (int) $this->Article->countArticles($query);


        // Calcul du nombre de pages à afficher
        $pages = ceil($nb_articles / self::PER_PAGE);

        // Calcul du premier article de la page
        $first = ($currentPage * self::PER_PAGE) - self::PER_PAGE;

        // On stocke la liste des articles dans $articles
        $articles = $this->Article->getAll($first, self::PER_PAGE, $query);

        // On génère la vue
        require_once(ROOT . 'views/articles/index.php');

        // On stocke le contenu dans $content
        $content = ob_get_clean();

        // On fabrique le "template"
        require_once(ROOT . 'views/index.php');
    }

    /**
     * Méthode permettant d'afficher un article à partir de son slug
     *
     * @param int $id
     * @return void
     */
    public function detail(int $id)
    {
        // On instancie le modèle "Article"
        $this->loadModel('Article');

        // On démarre le buffer de sortie
        ob_start();

        // On stocke l'article dans $article
        $article = $this->Article->getOne($id);

        // On génère la vue
        require_once(ROOT . 'views/articles/detail.php');

        // On stocke le contenu dans $content
        $content = ob_get_clean();

        // On fabrique le "template"
        require_once(ROOT . 'views/index.php');
    }

    public function create()
    {
        // On instancie le modèle "Article"
        $this->loadModel('Article');

        // On démarre le buffer de sortie
        ob_start();

        // On génère la vue
        require_once(ROOT . 'views/articles/create.php');

        // On stocke le contenu dans $content
        $content = ob_get_clean();

        // On fabrique le "template"
        require_once(ROOT . 'views/index.php');
    }

    public function createNewArticle()
    {

        // On instancie le modèle "Article"
        $this->loadModel('Article');

        $config = include('/var/www/html/config.php');
        $error = "";

        if (isset($_POST['article-title']) && isset($_POST['article-content']) && isset($_FILES['article-image']) && $_FILES['article-image']['error'] === UPLOAD_ERR_OK) {

            // Vérification du non dépassement des capacités en BDD
            if (strlen($_POST['article-title']) > 100) {
                $error = "Le titre est trop long, il doit faire moins de 100 caractères";
            } elseif (strlen($_POST['article-content']) > 3500 && strlen($_POST['article-content']) < 1) {
                $error = "Le contenu de l'article est trop long ou trop court, il doit faire moins de 1500 caractères";
            } else {

                // Traitement de l'image uploadée
                $fileTmpPath = $_FILES['article-image']['tmp_name'];
                $fileName = $_FILES['article-image']['name'];
                $fileSize = $_FILES['article-image']['size'];
                $fileType = $_FILES['article-image']['type'];
                $fileNameCmps = explode(".", $fileName);
                $fileExtension = strtolower(end($fileNameCmps));

                $newFileName = md5(time() . $fileName) . '.' . $fileExtension;

                // directory in which the uploaded file will be moved
                $uploadFileDir = '/var/www/html/' . $config['roots']['data'] . '/';
                $dest_path = $uploadFileDir . $newFileName;

                if (move_uploaded_file($fileTmpPath, $dest_path)) {
                    $message = "L'image a bien été upload";

                    // Alors on update l'Article
                    $newArticle = new Article();
                    $newArticle->createNew($_POST['article-title'], $_POST['article-content'], $newFileName, 1);
                    header('Location: /');
                } else {
                    $error = "Une erreur est survenue lors de la tentative de modification de l'article";
                }
            }
        } else {
            $error = "Tous les champs n'ont pas été remplis";
        }

        if(isset($error) && $error != '') { header('Location: /articles/create?error=' . $error); }
    }

    public function search(){
        // On instancie le modèle "Article"
        $this->loadModel('Article');
        $search = new Article();
        $request = $_POST['search'];
        $searchResult = $search->searchInArticles($request);

        // On démarre le buffer de sortie
        ob_start();

        // On envoie la liste des articles à rechercher
        $search = $this->Article->getAllByIds($searchResult);

        // On génère la vue
        require_once(ROOT . 'views/articles/search.php');

        // On stocke le contenu dans $content
        $content = ob_get_clean();

        // On fabrique le "template"
        require_once(ROOT . 'views/index.php');
    }
}
# Dockerfile
FROM php:7.4-apache

RUN apt-get update && apt-get install -y \
    && docker-php-ext-install pdo pdo_mysql \
    && docker-php-ext-enable pdo pdo_mysql
RUN a2enmod rewrite

ADD . /var/www/html